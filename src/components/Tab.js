import React, { PureComponent, Fragment } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import Color from '../utils/Color';

class Tab extends PureComponent {

    render() {
        const {
            isSelected,
            style,
            title,
            selectedColor,
            onPress
        } = this.props;

        return (
            <TouchableOpacity onPress={() => onPress(title)} activeOpacity={1.0} style={[styles.container, style]}>
                {isSelected && <View style={[styles.selectedTabContainer, { backgroundColor: selectedColor }]}>
                    <Text style={[styles.label, { color: Color.WHITE, fontSize: 14 }]}>
                        {title}
                    </Text>
                    <View style={styles.radioButtonContainer}>
                        <View style={[styles.selectedButton, { backgroundColor: selectedColor }]} />
                    </View>
                </View>}
                {!isSelected && <Fragment>
                    <Text style={styles.label}>
                        {title}
                    </Text>
                    <View style={styles.radioButtonContainer} />
                </Fragment>}
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 12,
        backgroundColor: '#cacaca',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    radioButtonContainer: {
        marginTop: 3,
        backgroundColor: Color.WHITE,
        alignItems: 'center',
        justifyContent: 'center',
        height: 20,
        width: 20,
        borderRadius: 10,
        elevation: 2
    },
    label: {
        textTransform: 'uppercase',
        fontWeight: '700',
        fontSize: 12,
        color: 'grey'
    },
    selectedButton: {
        backgroundColor: 'blue',
        borderRadius: 7,
        width: 12,
        height: 12
    },
    selectedTabContainer: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        top: -10,
        left: 0,
        right: 0, bottom: -16,
        backgroundColor: 'orange',
        borderTopLeftRadius: 10,
        borderBottomRightRadius: 50,
        elevation: 2
    }
})

export default Tab;