import React, { PureComponent } from 'react';
import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

const back = require('../../assets/images/ic_back.png');
const home = require('../../assets/images/ic_home.png');
const profile = require('../../assets/images/ic_avatar.png');

class Toolbar extends PureComponent {

    render() {

        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.backButton}>
                    <Image style={styles.backImage} source={back} />
                </TouchableOpacity>

                <Image resizeMode='contain'
                    style={styles.home}
                    source={home} />

                <Image style={styles.profile} source={profile} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 56,
        flexDirection: 'row',
        alignItems: 'center'
    },
    backButton: {
        marginLeft: 16
    },
    backImage: {
        width: 32,
        height: 32
    },
    home: {
        flex: 1,
        height: 32,
        alignSelf: 'center'
    },
    profile: {
        width: 48,
        height: 48,
        marginRight: 16,
    }
});

export default Toolbar;