export default {
    WHITE: '#FFFFFF',
    BLACK: '#000000',
    DARK_GREEN: '#018a4f',
    LIGHT_GREEN: '#10b932',
    BLUE: '#5ac0ee',
    YELLOW: '#fbc60e',
    LIGHT_GREY: '#e0e0e0'
}