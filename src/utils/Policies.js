export default {
    'Basic': [
        {
            name: 'Hospital costs',
            description: 'VND 2 million/ day, max. of VND 80 million',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Surgery costs',
            description: 'VND 2 million/ year',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Money in case of death',
            description: 'Max. of VND 80 million',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Money in case of total disability',
            description: 'Max. of VND 80 million',
            icon: require('../../assets/images/ic_patient.png')
        }
    ],
    'Advanced': [
        {
            name: 'Hospital costs',
            description: 'VND 2 million/ day, max. of VND 80 million',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Surgery costs',
            description: 'VND 2 million/ year',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Money in case of death',
            description: 'Max. of VND 80 million',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Money in case of total disability',
            description: 'Max. of VND 80 million',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Money in case of partial disability',
            description: 'Max. of VND 50 million',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Doctor visit costs',
            description: 'VND 3 million/year',
            icon: require('../../assets/images/ic_patient.png')
        }
    ],
    'Superior': [
        {
            name: 'Hospital costs',
            description: 'VND 2 million/ day, max. of VND 80 million',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Surgery costs',
            description: 'VND 2 million/ year',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Money in case of death',
            description: 'Max. of VND 80 million',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Money in case of partial disability',
            description: 'Max. of VND 50 million',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Doctor visit costs',
            description: 'VND 3 million/year',
            icon: require('../../assets/images/ic_patient.png')
        },
        {
            name: 'Dentist visit costs',
            description: 'VND 2 million/year',
            icon: require('../../assets/images/ic_patient.png')
        }
    ]
}

export const ProtectionAmount = {
    'Basic': 'VND 80 trieu',
    'Advanced': 'VND 100 trieu',
    'Superior': 'VND 120 trieu'
}

export const WhatYouPay = {
    'Basic': 'VND 800 ngan',
    'Advanced': 'VND 1.1 trieu',
    'Superior': 'VND 1.2 trieu'
}