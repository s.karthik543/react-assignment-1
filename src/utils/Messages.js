export default {
    POLICY_PLAN: 'We think this policy might suit you best - though do choose another if you prefer that',
    BASIC: 'Basic',
    ADVANCED: 'Advanced',
    SUPERIOR: 'Superior'
}