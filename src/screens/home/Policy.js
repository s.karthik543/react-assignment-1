import React, { PureComponent } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native';
import Color from '../../utils/Color';

class Policy extends PureComponent {

    render() {
        const { name, description, icon } = this.props.data;

        return (
            <View style={styles.container}>
                <Image source={icon} style={styles.logo} />
                <Text style={styles.title}>
                    {name}
                </Text>
                <Text style={styles.description}>
                    {description}
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 16,
        paddingHorizontal: 50
    },
    logo: {
        width: 24,
        height: 24
    },
    title: {
        fontSize: 14,
        textAlign: 'center',
        lineHeight: 20,
        fontWeight: 'bold',
        color: Color.BLACK,
        marginHorizontal: 40
    },
    description: {
        fontSize: 14,
        textAlign: 'center',
        lineHeight: 20,
        color: Color.BLACK
    }
});

export default Policy;