import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    ScrollView,
    SafeAreaView,
    TouchableOpacity
} from 'react-native';
import Toolbar from '../../components/Toolbar';
import Tab from '../../components/Tab';
import Policy from '../home/Policy';

import Messages from '../../utils/Messages';
import Color from '../../utils/Color';
import Polices, { WhatYouPay, ProtectionAmount } from '../../utils/Policies';

const rightArrow = require('../../../assets/images/ic_right_arrow.png');

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: Messages.BASIC,
            policies: Polices[Messages.BASIC],
            whatYouPay: WhatYouPay[Messages.BASIC],
            protectionAmount: ProtectionAmount[Messages.BASIC]
        }
    }

    onTabSelected = (selectedTab) => {
        this.setState({
            selectedTab,
            policies: Polices[selectedTab],
            whatYouPay: WhatYouPay[selectedTab],
            protectionAmount: ProtectionAmount[selectedTab]
        });
    }

    render() {
        const { selectedTab, policies, whatYouPay, protectionAmount } = this.state;

        return (
            <SafeAreaView style={styles.container}>
                <Toolbar />
                <Text style={styles.policyPlan} >
                    {Messages.POLICY_PLAN}
                </Text>

                <View style={styles.cardContainer}>
                    <View style={styles.tabContainer}>

                        <Tab title={Messages.BASIC}
                            selectedColor={Color.BLUE}
                            onPress={this.onTabSelected}
                            isSelected={selectedTab === Messages.BASIC}
                            style={{ borderTopLeftRadius: 10, backgroundColor: Color.LIGHT_GREY }} />

                        {selectedTab === Messages.SUPERIOR && <View style={{ width: 1 }} />}

                        <Tab title={Messages.ADVANCED}
                            selectedColor={Color.YELLOW}
                            isSelected={selectedTab === Messages.ADVANCED}
                            backgroundColor={Color.LIGHT_GREY}
                            onPress={this.onTabSelected}
                            style={{ backgroundColor: Color.LIGHT_GREY }} />

                        {selectedTab === Messages.BASIC && <View style={{ width: 1 }} />}

                        <Tab title={Messages.SUPERIOR}
                            selectedColor={Color.LIGHT_GREEN}
                            onPress={this.onTabSelected}
                            isSelected={selectedTab === Messages.SUPERIOR}
                            backgroundColor={Color.LIGHT_GREY}
                            style={{ borderTopRightRadius: 10, backgroundColor: selectedTab === Messages.SUPERIOR ? Color.WHITE : Color.LIGHT_GREY }} />
                    </View>

                    <ScrollView style={{ flex: 1, marginTop: 16 }} showsVerticalScrollIndicator={false}>
                        <Text style={styles.title}>
                            {'Single \nHealth'}
                        </Text>

                        <View style={styles.descriptionContainer}>
                            <View style={{ flex: 1, marginRight: 10 }}>
                                <Text style={styles.headerLabel}>
                                    What you pay
                                </Text>
                                <Text style={styles.headerValue}>
                                    {whatYouPay}
                                </Text>
                            </View>

                            <View style={{ flex: 1, marginLeft: 10 }}>
                                <Text style={styles.headerLabel}>
                                    Protection amount
                                </Text>
                                <Text style={styles.headerValue}>
                                    {protectionAmount}
                                </Text>
                            </View>
                        </View>

                        <View style={styles.descriptionContainer}>
                            <View style={{ flex: 1, marginRight: 10 }}>
                                <Text style={styles.headerLabel}>
                                    Insurer
                                </Text>
                                <Text style={styles.headerValue}>
                                    Bao Minh Corporation
                                </Text>
                            </View>

                            <View style={{ flex: 1, marginLeft: 10 }}>
                                <Text style={styles.headerLabel}>
                                    Amount of time you are covered
                            </Text>
                                <Text style={styles.headerValue}>
                                    1 year
                                </Text>
                            </View>
                        </View>

                        <Text style={styles.policyLabel}>
                            What's included
                        </Text>

                        {policies.map((item, index) => <Policy data={item} key={index} />)}

                        <TouchableOpacity style={styles.greenButton}>
                            <Text style={{ fontSize: 14, color: Color.WHITE, fontWeight: '700' }}>
                                Get this
                            </Text>
                        </TouchableOpacity>

                        <View style={styles.moreInfoContainer}>
                            <Text style={styles.moreInfoLabel}>
                                More Info
                            </Text>

                            <Image style={{ width: 24, height: 24 }} source={rightArrow} />
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.DARK_GREEN
    },
    policyPlan: {
        fontSize: 16,
        fontWeight: '600',
        color: Color.WHITE,
        marginHorizontal: 25,
        marginTop: 16,
        lineHeight: 24
    },
    cardContainer: {
        flex: 1,
        elevation: 2,
        backgroundColor: Color.WHITE,
        borderRadius: 10,
        marginHorizontal: 10,
        marginVertical: 16,
        // overflow: 'hidden'
    },
    tabContainer: {
        flexDirection: 'row',
        backgroundColor: Color.WHITE,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    greenButton: {
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        backgroundColor: 'green',
        borderRadius: 20,
        marginTop: 40
    },
    headerLabel: {
        fontSize: 12,
        color: Color.BLACK
    },
    headerValue: {
        lineHeight: 20,
        fontSize: 16,
        fontWeight: 'bold'
    },
    title: {
        marginLeft: 16,
        lineHeight: 20,
        marginTop: 10,
        fontSize: 16,
        fontWeight: 'bold'
    },
    descriptionContainer: {
        flexDirection: 'row',
        paddingHorizontal: 20,
        marginTop: 16
    },
    moreInfoContainer: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 20,
        marginTop: 10,
        marginBottom: 40
    },
    moreInfoLabel: {
        fontSize: 16,
        marginRight: 5,
        fontWeight: 'bold',
        color: Color.BLACK
    },
    policyLabel: {
        fontSize: 14,
        color: Color.BLACK,
        marginTop: 16,
        textAlign: 'center',
        fontWeight: 'bold'
    }
})

export default Home